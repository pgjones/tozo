provider "acme" {
  server_url = "https://acme-v02.api.letsencrypt.org/directory"
}

resource "tls_private_key" "private_key" {
  algorithm = "RSA"
}

resource "acme_registration" "philip" {
  account_key_pem = tls_private_key.private_key.private_key_pem
  email_address   = var.heroku_username
}

resource "acme_certificate" "tozo_dev" {
  account_key_pem = acme_registration.philip.account_key_pem
  common_name     = "tozo.dev"

  dns_challenge {
    provider = "gandiv5"

    config = {
      GANDIV5_API_KEY = var.gandi_api_key
    }
  }
}
