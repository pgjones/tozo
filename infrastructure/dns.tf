variable "gandi_api_key" {
  sensitive = true
}

provider "gandi" {
  key = var.gandi_api_key
}

data "gandi_domain" "tozo_dev" {
  name = "tozo.dev"
}

resource "gandi_livedns_record" "tozo_dev_ALIAS" {
  zone   = data.gandi_domain.tozo_dev.id
  name   = "@"
  type   = "ALIAS"
  ttl    = 3600
  values = [heroku_domain.tozo.cname]
}

resource "gandi_livedns_record" "tozo_dev_MX" {
  zone   = data.gandi_domain.tozo_dev.id
  name   = "@"
  type   = "MX"
  ttl    = 10800
  values = ["10 spool.mail.gandi.net.", "50 fb.mail.gandi.net."]
}

resource "gandi_livedns_record" "tozo_dev_SPF" {
  zone   = data.gandi_domain.tozo_dev.id
  name   = "@"
  type   = "TXT"
  ttl    = 10800
  values = ["\"v=spf1 include:_mailcust.gandi.net ?all\""]
}

resource "gandi_livedns_record" "tozo_dev_DKIM" {
  zone   = data.gandi_domain.tozo_dev.id
  name   = "20210807103031pm._domainkey"
  type   = "TXT"
  ttl    = 10800
  values = ["k=rsa;p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDm2rt5ln+UTNO+QzCyGVdbXp94n2LJkr8UJEjAs599g0kaSjZ/jzfIXASdrCpOwOOMTwp4d1pBwlCmop2p1PDrSAgQCrwOoTWI3DQPn+YXOt7vXAVQnl+WKoZvEnUc3vaOBxqgenPL6gLAp91B0j0Jvwh+9scZme+IjGLszwO+DwIDAQAB"]
}

resource "gandi_livedns_record" "tozo_dev_CNAME" {
  zone   = data.gandi_domain.tozo_dev.id
  name   = "pm-bounces"
  type   = "CNAME"
  ttl    = 10800
  values = ["pm.mtasv.net."]
}
