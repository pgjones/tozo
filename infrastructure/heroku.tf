variable "heroku_api_key" {
  sensitive = true
}

variable "heroku_username" {
  sensitive = true
}

variable "postmark_token" {
  sensitive = true
}

variable "secret_key" {
  sensitive = true
}

provider "heroku" {
  email   = var.heroku_username
  api_key = var.heroku_api_key
}

resource "heroku_app" "tozo" {
  name   = "tozo"
  region = "eu"
  stack  = "container"

  config_vars = {
    TOZO_BASE_URL                 = "https://tozo.dev"
    TOZO_QUART_AUTH_COOKIE_NAME   = "__Host-TozoSession-1"
    TOZO_QUART_AUTH_COOKIE_SECURE = "true"
  }

  sensitive_config_vars = {
    TOZO_POSTMARK_TOKEN = var.postmark_token
    TOZO_SECRET_KEY     = var.secret_key
  }
}

resource "heroku_addon" "tozo-db" {
  app_id = heroku_app.tozo.id
  plan   = "heroku-postgresql:hobby-dev"
}

resource "heroku_formation" "tozo-web" {
  app_id   = heroku_app.tozo.id
  type     = "web"
  quantity = 1
  size     = "Hobby"
}

resource "heroku_domain" "tozo" {
  app_id          = heroku_app.tozo.id
  hostname        = "tozo.dev"
  sni_endpoint_id = heroku_ssl.tozo_dev.id
}

resource "heroku_ssl" "tozo_dev" {
  app_id            = heroku_app.tozo.uuid
  certificate_chain = "${acme_certificate.tozo_dev.certificate_pem}${acme_certificate.tozo_dev.issuer_pem}"
  private_key       = acme_certificate.tozo_dev.private_key_pem

  depends_on = [heroku_formation.tozo-web]
}
