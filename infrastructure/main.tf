terraform {
  required_providers {
    acme = {
      source  = "vancluever/acme"
      version = "~> 2.0"
    }
    gandi = {
      version = "2.0.0-rc2-fork-3"
      source  = "manvalls/gandi"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">=3.3.0"
    }
    heroku = {
      source  = "heroku/heroku"
      version = ">=5.0.2"
    }
  }
  required_version = ">=0.14"
}
