variable "gitlab_token" {
  sensitive = true
}

provider "gitlab" {
  token = var.gitlab_token
}

resource "gitlab_project" "tozo" {
  name                   = "tozo"
  visibility_level       = "public" # You likely want private here
  default_branch         = "main"
  shared_runners_enabled = true
}

resource "gitlab_pipeline_schedule" "tozo" {
  project     = gitlab_project.tozo.id
  description = "Schedule checks against the codebase"
  ref         = "main"
  cron        = "0 9 1 * *"
}

resource "gitlab_project_variable" "heroku_app" {
  key       = "HEROKU_APP"
  value     = heroku_app.tozo.name
  project   = gitlab_project.tozo.id
  protected = true
}
resource "gitlab_project_variable" "heroku_token" {
  key       = "HEROKU_API_KEY"
  value     = var.heroku_api_key
  project   = gitlab_project.tozo.id
  protected = true
}
resource "gitlab_project_variable" "heroku_username" {
  key       = "HEROKU_USERNAME"
  value     = var.heroku_username
  project   = gitlab_project.tozo.id
  protected = true
}
