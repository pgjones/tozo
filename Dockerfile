FROM node:16-alpine as frontend

WORKDIR /frontend/

COPY frontend/package.json frontend/package-lock.json /frontend/
RUN npm install

COPY frontend /frontend/
RUN npm run build

FROM python:3.10-alpine

WORKDIR /app
COPY start hypercorn.toml /app/
ENTRYPOINT ["dumb-init"]
CMD ["./start"]

RUN apk --no-cache add alpine-sdk build-base cargo gcc git libffi-dev \
    musl-dev openssl openssl-dev

RUN python -m venv /ve
ENV PATH=/ve/bin:${PATH}

RUN pip install --no-cache-dir dumb-init poetry

RUN mkdir -p /app/backend/static /app/backend/templates /root/.config/pypoetry

COPY backend/poetry.lock backend/pyproject.toml /app/
RUN poetry config virtualenvs.create false \
    && poetry install --no-root \
    && poetry cache clear pypi --all --no-interaction

COPY --from=frontend /frontend/build/index.html /app/backend/templates/
COPY --from=frontend /frontend/build/static/. /app/backend/static/
COPY --from=frontend /frontend/build/*.js* /app/backend/static/
COPY --from=frontend /frontend/build/*.png /frontend/build/*.svg /app/backend/static/

COPY backend/src/ /app/

USER nobody
