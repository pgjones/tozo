import os
import sys
from subprocess import call, CalledProcessError, check_call, DEVNULL
from urllib.parse import urlparse

from dotenv import load_dotenv

def _check_call_quiet(
    commands: list[str], *, shell: bool=False
) -> None:
    try:
        check_call(commands, shell=shell)
    except CalledProcessError as error:
        sys.exit(error.returncode)


def format() -> None:
    _check_call_quiet(["black", "src/", "tests/"])
    _check_call_quiet(["isort", "src", "tests"])
    _check_call_quiet(
        "find src/ -name *.html | xargs djhtml -t 2 --in-place",
        shell=True,
    )

def lint() -> None:
    _check_call_quiet(
        ["black", "--check", "--diff", "src/", "tests/"]
    )
    _check_call_quiet(
        ["isort", "--check", "--diff", "src", "tests"]
    )
    _check_call_quiet(
        "find src/ -name *.html | xargs djhtml -t 2 --check",
        shell=True,
    )
    _check_call_quiet(["mypy", "src/backend/", "tests/"])
    _check_call_quiet(["flake8", "src/", "tests/"])
    _check_call_quiet(["vulture", "src/"])
    _check_call_quiet(["bandit", "-r", "src/"])

def test() -> None:
    load_dotenv("testing.env")
    load_dotenv("vars.env")
    recreate_db()
    _check_call_quiet(["pytest", "tests/"])


def create_db() -> None:
    db_url = urlparse(os.environ["TOZO_QUART_DB_DATABASE_URL"])
    call(
        ["psql", "-U", "postgres", "-c", f"CREATE USER {db_url.username} LOGIN PASSWORD '{db_url.password}' CREATEDB"],
        stdout=DEVNULL,
        stderr=DEVNULL,
    )
    call(
        ["psql", "-U", "postgres", "-c", f"CREATE DATABASE {db_url.path.removeprefix('/')}"],
        stdout=DEVNULL,
        stderr=DEVNULL,
    )

def drop_db() -> None:
    db_url = urlparse(os.environ["TOZO_QUART_DB_DATABASE_URL"])
    call(
        ["psql", "-U", "postgres", "-c", f"DROP DATABASE {db_url.path.removeprefix('/')}"],
        stdout=DEVNULL,
        stderr=DEVNULL,
    )
    call(
        ["psql", "-U", "postgres", "-c", f"DROP USER {db_url.username}"],
        stdout=DEVNULL,
        stderr=DEVNULL,
    )

def recreate_db() -> None:
    load_dotenv("vars.env")
    drop_db()
    create_db()

def start() -> None:
    load_dotenv("vars.env")
    _check_call_quiet(["python", "src/backend/run.py"])
