from unittest.mock import AsyncMock

from _pytest.monkeypatch import MonkeyPatch
from quart import Quart

import backend.blueprints.members


async def test_registration_flow(app: Quart, monkeypatch: MonkeyPatch) -> None:
    mail_client = AsyncMock()
    monkeypatch.setattr(backend.blueprints.members, "send_email", mail_client)

    test_client = app.test_client()
    data = {
        "email": "new@example.test",
        "password": "testPassword2$",
    }
    await test_client.post("/members/", json=data)
    response = await test_client.post("/sessions/", json=data)
    assert response.status_code == 200
    assert mail_client.await_args_list[0][0][0] == "new@example.test"

    token_data = mail_client.await_args_list[0][0][3]
    response = await test_client.put("/members/email/", json=token_data)
    assert response.status_code == 200


async def test_change_password(app: Quart) -> None:
    test_client = app.test_client()
    data = {
        "email": "new_password@example.test",
        "password": "testPassword2$",
    }
    response = await test_client.post("/members/", json=data)
    async with test_client.authenticated("2"):  # type: ignore
        response = await test_client.put(
            "/members/password/",
            json={
                "currentPassword": data["password"],
                "newPassword": "testPassword3$",
            },
        )
        assert response.status_code == 200
