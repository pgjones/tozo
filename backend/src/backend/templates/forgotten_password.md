Hello,

You can use this [link]({{ config["BASE_URL"] }}/reset-password/{{ token }}/) to reset your password.

Thanks
