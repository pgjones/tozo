Hello and welcome to tozo!

Please confirm you signed up by following this [link]({{ config["BASE_URL"] }}/confirm-email/{{ token }}/).

Thanks
