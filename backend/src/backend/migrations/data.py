from quart_db import Connection


async def execute(connection: Connection) -> None:
    await connection.execute(
        """INSERT INTO members (email, password_hash)
                VALUES ('member@tozo.test', '$2b$14$MZEMykkZJ2ORODr3fK2ZFOjNqlQ4B8Js7Qwl5b88zhEpPv05K7KJO')"""  # noqa: E501
    )
