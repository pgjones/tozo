from typing import Any

from markdown import Markdown as BaseMarkdown


def to_plain_string(element: Any) -> str:
    result = ""
    if element.tag == "a":
        result = f"{element.text} ({element.attrib['href']})"
    elif element.text:
        result = element.text

    for node in element:
        result += to_plain_string(node)

    if element.tail:
        result += element.tail
    return result


class Markdown(BaseMarkdown):
    def __init__(self, **kwargs: Any) -> None:
        self.output_formats["plain"] = to_plain_string  # type: ignore
        super().__init__(**kwargs)
        if self.output_format == "plain":
            self.stripTopLevelTags = False
